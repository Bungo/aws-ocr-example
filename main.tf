
resource "aws_iam_role" "lambda_role" {
  #Watch out! For the IT-Days we use managed-policies (especially the DynamoDBFullAccess) for simplicity reasons. This is not according to the principle of least privilege and should be handled differently in production!
  name                = "test_role"
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonTextractFullAccess", "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"]
  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Action : "sts:AssumeRole",
        Principal : {
          Service : "lambda.amazonaws.com"
        },
        Effect : "Allow",
        Sid : ""
      }
    ]
  })

}

resource "aws_dynamodb_table" "form_table" {
  name         = "formdata"
  hash_key     = "DocId"
  billing_mode = "PAY_PER_REQUEST" #Otherwise it is not serverless

  attribute {
    name = "DocId"
    type = "S"
  }
}

resource "aws_lambda_function" "lambda" {
  depends_on = [
    data.archive_file.lambda_file
  ]
  filename      = "./assets/lambda/lambda_function.zip"
  function_name = "Img2Text"
  role          = aws_iam_role.lambda_role.arn
  handler       = "lambda_function.lambda_handler"
  timeout       = 60


  runtime = "python3.9"

  environment {
    variables = {
      DYNAMO = aws_dynamodb_table.form_table.name
    }
  }
}

resource "aws_api_gateway_rest_api" "rest_interface" {
  name               = "ITDays"
  binary_media_types = ["image/jpeg", "image/png"]
}

resource "aws_api_gateway_resource" "form_path" {
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
  parent_id   = aws_api_gateway_rest_api.rest_interface.root_resource_id
  path_part   = "form"
}

resource "aws_api_gateway_request_validator" "request_validator" {
  name                        = "Validate body, query string parameters, and headers"
  rest_api_id                 = aws_api_gateway_rest_api.rest_interface.id
  validate_request_body       = true
  validate_request_parameters = true
}

resource "aws_api_gateway_method" "put_method" {
  rest_api_id          = aws_api_gateway_rest_api.rest_interface.id
  resource_id          = aws_api_gateway_resource.form_path.id
  http_method          = "PUT"
  authorization        = "NONE" #Definitely not best practise for sensitive data and except such IT-Days, you should NOT go withou authorization for sensitive data
  request_parameters   = { "method.request.querystring.id" = false }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_method" "get_method" {
  rest_api_id          = aws_api_gateway_rest_api.rest_interface.id
  resource_id          = aws_api_gateway_resource.form_path.id
  http_method          = "GET"
  authorization        = "NONE" #Definitely not best practise for sensitive data and except such IT-Days, you should NOT go withou authorization for sensitive data
  request_parameters   = { "method.request.querystring.id" = true }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_integration" "integration_rest" {
  for_each                = toset(["GET", "PUT"])
  rest_api_id             = aws_api_gateway_rest_api.rest_interface.id
  resource_id             = aws_api_gateway_resource.form_path.id
  http_method             = each.value
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda.invoke_arn
}

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [
    aws_api_gateway_integration.integration_rest
  ]
  rest_api_id = aws_api_gateway_rest_api.rest_interface.id
}

resource "aws_api_gateway_stage" "stage" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.rest_interface.id
  stage_name    = "default"
}

resource "aws_lambda_permission" "api_lambda_invoke_permissions" {
  for_each      = toset(["GET", "PUT"])
  statement_id  = "AllowExecutionFromAPIGateway${each.value}"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${local.region}:${local.account_id}:${aws_api_gateway_rest_api.rest_interface.id}/*/${each.value}${aws_api_gateway_resource.form_path.path}"
}



