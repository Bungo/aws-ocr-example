import boto3
import json
import uuid
import os
import base64

dynamo = boto3.client('dynamodb')
textract = boto3.client('textract')
dynamo_fields = ['Vorname']

def lambda_handler(event, context):
    http_method = event['httpMethod']
    id = uuid.uuid4().hex if event['queryStringParameters'] == None else event['queryStringParameters']['id']
    
    if http_method == 'PUT':
        return put(id, event)
    else:
        return get(id)
    
    return response
    
def get(id):
    response = dynamo.get_item(
        TableName=os.environ['DYNAMO'],
        Key = {
            'DocId': {'S':id} #DynamoDB works with Typeidentifier keys like 'S' for String, 'L' for List etc.
        }
    )
    status = 200 if 'Item' in response else 404
    body = {'message':'No Databaseentry with the specified ID'} if status == 404 else response['Item']
    return {
            'statusCode': status,
            'headers': {
                'Content-Type': 'application/json',
            },
            'body': json.dumps(body)
    }


def put(id, event):
    headers = dict((k.lower(), v) for k, v in event['headers'].items())
    if event['body'] is None:
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
            },
            'body': json.dumps({
                'message': 'No body provided!'
            })
        }
    if headers['content-type'] not in ['image/png', 'image/jpeg']:
        return {
            'statusCode': 400,
            'headers': {
                'Content-Type': 'application/json',
            },
            'body': json.dumps({
                'message': 'No valid content-type!'
            })
        }
    response = textract.analyze_document(
        Document={
            'Bytes': base64.b64decode(event['body'])
        },
        FeatureTypes=[
            'FORMS'
        ]
    )
    #Check documentation to understand the response format = https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/textract.html#Textract.Client.analyze_document
    
    dynamo_item = _form_blocks(response['Blocks'])
    dynamo_item['DocId'] = {'S':id} #DynamoDB works with Typeidentifier keys like 'S' for String, 'L' for List etc.
    dynamo.put_item(
        TableName=os.environ['DYNAMO'],
        Item=dynamo_item
    )
    return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'application/json',
            },
            'body': json.dumps(dynamo_item)
        }
    
def _form_blocks(blocks):
    lines = {}
    keys = {}
    values = {}
    checkboxes = {}
    for block in blocks:
        if 'Relationships' in block:
            if block['BlockType'] == 'LINE':
                block_id = block['Relationships'][0]['Ids'][0]
                lines[block_id] = block['Text']
            elif block['BlockType'] == 'KEY_VALUE_SET':
                entry = {}
                for map in block['Relationships']:
                    entry[map['Type']] = map['Ids'][0]
                if block['EntityTypes'][0] == 'KEY':
                    if 'CHILD' in entry and 'VALUE' in entry:
                        block_id = entry['CHILD']
                        keys[block_id] = entry['VALUE']
                else:
                     if 'CHILD' in entry:
                        block_id = block['Id']
                        values[block_id] = entry['CHILD']
        elif block['BlockType'] == 'SELECTION_ELEMENT':
            block_id = block['Id']
            checkboxes[block_id] = block['SelectionStatus']
    return _create_map(lines, keys, values, checkboxes)
    
def _create_map(lines, keys, values, checkboxes):
    final_dict = {'Anmeldungen':{'L':[]}} #DynamoDB works with Typeidentifier keys like 'S' for String, 'L' for List etc.
    print(keys)
    for key, child in keys.items():
        if key in lines and child in values:
            key_name = lines[key]
            value_id = values[child]
            if value_id in checkboxes:
                if checkboxes[value_id] == 'SELECTED':
                    final_dict['Anmeldungen']['L'].append({'S':key_name}) #DynamoDB works with Typeidentifier keys like 'S' for String, 'L' for List etc.
            elif value_id in lines:
                final_dict[key_name] = {'S':lines[value_id]}
    return final_dict
