data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "archive_file" "lambda_file" {
  type        = "zip"
  source_file = "./assets/lambda/lambda_function.py"
  output_path = "./assets/lambda/lambda_function.zip"
}
