# aws-ocr-example

This Gitlab Repository was created for demonstration purposes. It is used to show students the capabilities of AWS and what the jobtarget "Cloud Engineer" or "Data Scientist" actually could mean.

## 1. Download and install "awscli" from AWS and "terraform" from Hashicorp

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
https://learn.hashicorp.com/tutorials/terraform/install-cli

## 2. Create an AWS Account

https://aws.amazon.com/


## 3. On AWS create an IAM User Access Key + Secret Access Key

https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html

## 4. On Terminal: aws configure -> Set Access Keys + region (i did eu-central-1 for Frankfurt) + output format => json

https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html

## 5. Configure your AWS Access either via profile or other option (On other options you might need to append the provider.tf file)

https://registry.terraform.io/providers/hashicorp/aws/latest/docs

## 6. Go into the project root folder and type terraform apply => Accept the changes => Have fun playing arround with the Demo-Use-Case on AWS

https://www.terraform.io/cli/commands/apply
